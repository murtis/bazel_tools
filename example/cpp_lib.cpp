#include "example/cpp_lib.h"

namespace example_lib {

std::string constructString() { return std::string{"Hello world! "}; }

}  // example_lib
